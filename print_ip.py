from rpi_lcd import LCD
import urllib.request
import socket
from time import sleep

lcd = LCD()

def hello():
	lcd.text("Hello, welcome to uNode", 1)
	sleep(2)
	lcd.clear()

def printIP():
	lcd.text("Local IP:", 1)
	lcd.text("...", 2)
	lcd.text("External IP:", 3)
	lcd.text("...", 4)
	try:
		# get local IP
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		s.connect(("1.1.1.1", 80))
		local_ip = s.getsockname()[0]
		s.close()
		lcd.text(local_ip, 2)
		# get external IP	
		external_ip = urllib.request.urlopen("https://ifconfig.me").read().decode("utf8")
		lcd.text(external_ip, 4)
	except:
		print("couldn't get IP")

try:
	hello()
	printIP()
except KeyboardInterrupt:
	print("Error in LCD IP printing")
